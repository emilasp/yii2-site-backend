<?php
namespace emilasp\site\backend\extensions\topmenu;

use yii;
use yii\helpers\Url;
use yii\base\Widget;

/**
 * Class TopMenu
 * @package emilasp\site\backend\extensions\topmenu
 */
class TopMenu extends Widget
{

    const CACHE_PREFIX = 'user_menu:';

    public $menuPath = '@app/config/menu/menu.php';
    public $menus = [];

    private $action;
    private $controller;
    private $module;
    private $user_id;

    public function init()
    {
        $this->registerAssets();

        $this->action     = Yii::$app->controller->action->id;
        $this->controller = Yii::$app->controller->id;
        $this->module     = Yii::$app->controller->module->id;
        $this->user_id    = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : 'guest';
    }

    public function run()
    {
        $items = include(Yii::getAlias($this->menuPath));
        $items = $this->removeByRight($items);

        //$items = $this-> setActiveItems( $items );

        $menu = [['options' => ['class' => 'navbar-nav'], 'items' => $items]];

        echo $this->render('tmenu', ['menus' => $menu]);
    }

    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        TopMenuAsset::register($view);
    }


    /** Убираем из списка меню все элементы не проходящие по правам
     *
     * @param $items
     *
     * @return mixed
     */
    private function removeByRight($items)
    {
        foreach ($items as $index => $item) {
            if (is_array($item)) {
                if (isset($item['items'])) {
                    $items[$index]['items'] = $this->removeByRight($item['items']);

                    if (count($items[$index]['items']) == 0) {
                        unset($items[$index]);
                    }
                } else {
                    if (isset($item['role'])) {
                        $isAllowRole = false;
                        foreach ((array)$item['role'] as $role) {
                            if (Yii::$app->user->can($role)) {
                                $isAllowRole = true;
                            }
                        }
                        if (!$isAllowRole) {
                            unset($items[$index]);
                        }
                    }

                    if (isset($item['rule']) && is_callable($item['rule'])) {
                        $rule = $item['rule'];
                        if ($rule() !== true) {
                            unset($items[$index]);
                        }
                    }

                    /*$action = Yii::$app->rights->getNameAction($this->action, $this->controller, $this->module);

                    if (isset($items[$index])) {
                        foreach ($items[$index]['role'] as $role) {
                            if ($action == $role) {
                                $items[$index]['active'] = true;
                                break;
                            }
                        }
                        $items[$index]['url'] = Url::toRoute($items[$index]['url']);
                    }*/
                }
            }
        }
        return $items;
    }

    /** Рекурсивно выставляем активные пункты меню
     *
     * @param $items
     *
     * @return mixed
     */
    /* private function setActiveItems( $items ){
         foreach( $items as $index=>$item ){
             if(is_array($item)){
                 if(isset($item['items'])){
                     $items[$index]['items'] = $this->setActiveItems($item['items']);
                 }else{
                     $action = Yii::$app->rights->getNameAction( $this->action, $this->controller, $this->module );

                     foreach($items[$index]['role'] as $role){
                         if( $action==$role ){
                             $items[$index]['active'] = true;
                             break;
                         }
                     }
                 }
             }
         }
         return $items;
     }*/
}
