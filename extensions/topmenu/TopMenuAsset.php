<?php
namespace emilasp\site\backend\extensions\topmenu;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class TopMenuAsset
 * @package emilasp\site\backend\extensions\topmenu
 */
class TopMenuAsset extends AssetBundle
{
    public $jsOptions = ['position' => View::POS_END];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    //public $sourcePath = '@app/themes/neon/';

    public $css = [
        'tmenu.css'
    ];

    public $js = [];
}
