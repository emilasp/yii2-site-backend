<?php
/**
 * Created by PhpStorm.
 * User: emilasp
 */

namespace frontend\modules\site\extensions\breadcrumbs;

use yii;

/**
 * Расширение для вывода хлебных крошек
 *
 * Class Breadcrumbs
 * @package frontend\modules\site\extensions\breadcrumbs
 */
class Breadcrumbs  extends \yii\base\Widget {


    public $first;
    public $flat;

    public $items;

    public function init(){

        if(Yii::$app->user->isGuest)
            $homeLink = Yii::$app->params['defaultHomeLink'];
        else
            $homeLink = Yii::$app->user->identity->homeLink;

        if( !$homeLink ) $homeLink = '/';

        $this->items = yii\helpers\ArrayHelper::merge(['first'=>['url'=>yii\helpers\Url::toRoute($homeLink),'label'=>Yii::t('site','Home')]],$this->items);
        $this->registerAssets();

    }


    public function run(){
        echo $this->render('breadcrumbs', ['items'=>$this->items]);
    }


    /**
     * Register client assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        BreadcrumbsAsset::register($view);

     /*     $js =
     <<<JS

     $( document ).ready(function() {

     });


     JS;
             $view->registerJs($js,yii\web\View::POS_READY);*/

    }



}
