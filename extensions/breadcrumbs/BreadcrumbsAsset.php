<?php

namespace frontend\modules\site\extensions\breadcrumbs;


use frontend\modules\core\base\Asset;
use yii\web\View;

class BreadcrumbsAsset extends Asset
{
    public $jsOptions=['position'=>View::POS_HEAD];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        //$this->setupAssets('js', ['breadcrumbs']);
        //$this->setupAssets('css', ['breadcrumbs']);
        parent::init();
    }

}
