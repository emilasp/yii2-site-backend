<?php
namespace emilasp\site\backend;

use emilasp\core\CoreModule;

/**
 * Class SiteBackendModule
 * @package emilasp\site\backend
 */
class SiteBackendModule extends CoreModule
{
    public $controllerNamespace = 'emilasp\site\backend\controllers';

    public $menu;

    public function init()
    {
        parent::init();
    }
}
