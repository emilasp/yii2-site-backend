<?php
namespace emilasp\site\backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * SOrderController implements the CRUD actions for STool model.
 */
class SiteController extends Controller
{

    /*public function behaviors()
    {
        return array(
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['error', 'captcha', 'page'],
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['error', 'page', 'captcha'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        );
    }*/

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
        }
        return $this->render('index');
    }

}
